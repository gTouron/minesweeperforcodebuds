import { createApp } from 'vue'
import App from './App.vue'
import {firestorePlugin} from 'vuefire'

createApp(App,firestorePlugin).mount('#app')
