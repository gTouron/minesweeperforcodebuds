import fb from 'firebase';
import 'firebase/firestore';

var firebaseConfig = {
    apiKey: "AIzaSyCfGghRarUh0vLNOfEQbjmYwKzUz8TfzwU",
    authDomain: "minesweeper-b3846.firebaseapp.com",
    databaseURL: "https://minesweeper-b3846.firebaseio.com",
    projectId: "minesweeper-b3846",
    storageBucket: "minesweeper-b3846.appspot.com",
    messagingSenderId: "1019495405271",
    appId: "1:1019495405271:web:faf7e570eb6b9786e9c292"
  };
  // Initialize Firebase
export const db = fb.initializeApp(firebaseConfig).firestore();